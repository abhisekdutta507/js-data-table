const locations = [
  'Kolkata',
  'Pune',
  'Delhi',
  'Chennai',
  'Puri',
  'Kanpur'
];

const employees = [
  {
    rank: 1,
    firstName: 'Aarav',
    lastName: 'Sharma',
    age: 25,
    livesIn: 'Kolkata'
  },
  {
    rank: 2,
    firstName: 'Tania',
    lastName: 'Mishra',
    age: 24,
    livesIn: 'Pune'
  },
  {
    rank: 3,
    firstName: 'Puja',
    lastName: 'Gupta',
    age: 26,
    livesIn: 'Delhi'
  },
  {
    rank: 4,
    firstName: 'Nitin',
    lastName: 'Hegde',
    age: 27,
    livesIn: 'Chennai'
  },
  {
    rank: 5,
    firstName: 'Ananya',
    lastName: 'Hota',
    age: 23,
    livesIn: 'Puri'
  },
  {
    rank: 6,
    firstName: 'Utkarsh',
    lastName: 'Ojha',
    age: 26,
    livesIn: 'Puri'
  },
  {
    rank: 7,
    firstName: 'Hemanth',
    lastName: 'Tomar',
    age: 28,
    livesIn: 'Puri'
  },
];

const datatable = {
  columns: [
    {
      heading: '#',
      field: 'rank'
    },
    {
      heading: 'First Name',
      field: 'firstName'
    },
    {
      heading: 'Last Name',
      field: 'lastName'
    },
    {
      heading: 'Location',
      field: 'livesIn'
    }
  ]
};

const locationElementsSelector = '#location-selector';
const datatableElementsSelector = '#employee-list-selector';

const renderDatataleWithFilteredLocation = (location = '') => {
  const nextEmployees = employees.filter(({ livesIn }) => livesIn === location);
  renderDatatable(datatableElementsSelector, datatable, nextEmployees);
};

const locationChange = ({ target: { value } }) => {
  renderDatataleWithFilteredLocation(value);
};

const bindEvents = () => {
  // bind selector on change event
  const locationElements = [...document.querySelectorAll(locationElementsSelector)];
  if (locationElements.length) {
    locationElements.forEach((element) => element.addEventListener('change', locationChange));
  }
};

const renderSelector = (selector = locationElementsSelector, options = [], defaultSelectedOptionIndex = 0) => {
  const elements = [...document.querySelectorAll(selector)];
  const _options = [...new Set(options)];

  // empty the selector first
  elements.forEach((element) => element.innerHTML = '');

  const _optionsNodeList = _options.map((option) => {
    const op = document.createElement('option');
    op.value = option;
    op.textContent = option;
    return op;
  });

  elements.forEach((element) => element.append(..._optionsNodeList));
};

const renderDatatable = (selector = datatableElementsSelector, config = datatable, data = employees, styles = {
  theme: {
    thead: 'table-dark'
  }
}) => {
  const elements = [...document.querySelectorAll(selector)];
  const columns = [...new Set(config.columns)];

  // empty the datatable first
  elements.forEach((element) => element.innerHTML = '');

  // generate thead
  const thead = document.createElement('thead');
  thead.classList.add([styles.theme.thead]);
  const tr = document.createElement('tr');
  const thNodeList = columns.map((option) => {
    const th = document.createElement('th');
    th.setAttribute('scope', 'col');
    th.textContent = option.heading;
    return th;
  });
  tr.append(...thNodeList);
  thead.append(tr);

  // generate tbody
  const tbody = document.createElement('tbody');
  if (data.length) {
    const trNodeList = data.map((object) => {
      const tr = document.createElement('tr');
      const tdNodeList = columns.map((option, index) => {
        const td = document.createElement('td');
        if (index === 0) {
          td.setAttribute('scope', 'row');
        }
        td.textContent = object[option.field] || '';
        return td;
      });
      tr.append(...tdNodeList);
      return tr;
    });
    tbody.append(...trNodeList);
  } else {
    const tr = document.createElement('tr');
    const td = document.createElement('td');
    td.setAttribute('scope', 'row');
    td.setAttribute('colspan', columns.length);
    td.textContent = 'No data to display';
    tr.append(td);
    tbody.append(tr);
  }

  // append thead & tbody
  elements.forEach((element) => element.append(thead, tbody));
};

// start rendering datatable from here
const ready = (e) => {
  renderSelector(locationElementsSelector, locations, 0);
  renderDatataleWithFilteredLocation(locations[0]);
  bindEvents();
};

document.addEventListener('DOMContentLoaded', ready);